from collections import Counter  #;
from itertools import tee  #;
with open("day3.txt") as file:
#{
    v = [x.strip() for x in file]  #;
#}
gamma = "".join(map(lambda d: max(d, key=d.get), (Counter(x) for x in zip(*v))))  #;
epsilon = "".join(  #;
#{
    map(lambda d: min(d, key=d.get), (Counter(x) for x in zip(*v)))  #;
#}
)  #;
def read_file():
#{
    with open("day3.txt") as file:
    #{
        yield from (x.strip() for x in file)  #;
    #}
#}
def get_rating(is_oxygen, lst=None, cnt=None):
#{
    if lst is None:
    #{
        lst = read_file()  #;
    #}
    lst1, lst2, lst3 = tee(lst, 3)  #;
    if cnt is None:
    #{
        cnt = next(lst1)  #;
    #}
    elif not cnt:
    #{
        return next(lst1)  #;
    #}
    s = Counter((x[0] for x in lst2))  #;
    bit = [min, max][is_oxygen](s, key=lambda x: (s[x], x))  #;
    return get_rating(  #;
    #{
        is_oxygen,  #;
        [x[1:] + x[0] for x in lst3 if x[0] == bit],  #;
        cnt[:-1]  #;
    #}
    )  #;
#}
co2, oxygen = map(get_rating, range(2))  #;
print(oxygen)  #;
print(co2)  #;
print(int(oxygen, 2) * int(co2, 2))  #;
